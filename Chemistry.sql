USE master;

IF DB_ID('Pharmacy') IS NOT NULL DROP DATABASE Pharmacy;
IF @@ERROR = 3702 
   RAISERROR('Database cannot be dropped because there are still open connections.', 127, 127) WITH NOWAIT, LOG;

CREATE DATABASE Pharmacy;
GO


USE Pharmacy;
GO

DROP TABLE IF EXISTS Users
GO
CREATE TABLE Users
(
	UserId int NOT NULL IDENTITY(1,1),
	UserName nvarchar(101) NULL,
	Login nvarchar(101) NULL,
	Password nvarchar(101) NULL,
 CONSTRAINT PK_UserId PRIMARY KEY CLUSTERED (UserId ASC)
)
GO


DROP TABLE IF EXISTS MedicinesInfo
GO
CREATE TABLE MedicinesInfo
(
	MedicinesId int NOT NULL IDENTITY(1,1),
	Producer nvarchar(101) NULL,
	Name nvarchar(101) NULL,
	Type nvarchar(101) NULL,
	Price int NULL
 CONSTRAINT PK_MedicinesId PRIMARY KEY CLUSTERED (MedicinesId ASC)
)
GO

select Producer from MedicinesInfo group by Producer

select Type from MedicinesInfo group by Type

select * from MedicinesInfo

INSERT INTO MedicinesInfo (Producer, Name, Type, Price) VALUES ('Test', 'Nmae1', 'Pils', 400)