﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for SearchForm.xaml
    /// </summary>
    public partial class SearchForm : Window
    {
       
        

        public SearchForm()
        {
            InitializeComponent();
        }

        
        

      

        public void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            
           


            if (nameTxt.Text == "" & manTxt.Text == "")

            {
                MessageBox.Show("Invalid request");
                return;

            }

            if (nameTxt.Text == "" == false & manTxt.Text == "" == false)

                {
                MessageBox.Show("Use only one option");
                return;

            }

        }

        private void ButtonSignOut_Click(object sender, RoutedEventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
            this.Close();
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
            this.Close();
        }

        private void ButtonFil_Click(object sender, RoutedEventArgs e)
        {
            FilForm filForm = new FilForm();
            filForm.Show();
            this.Close();
        }

      

        private void cartTxt_Click(object sender, RoutedEventArgs e)
        {

            if(price.Text == "")

            {
                MessageBox.Show("Invalid request");
                return;
            }


            CheckOutForm checkOutForm = new CheckOutForm();
            checkOutForm.Cos.Text = this.price.Text;


           
                checkOutForm.Show();
                this.Close();

        }

        private void Dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid Dg = (DataGrid)sender;
        DataRowView row_selected = Dg.SelectedItem as DataRowView;
            if (row_selected != null)
            {

                price.Text = row_selected["Ціна"].ToString();
        }

       
    }

        private void nameTxt_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }

        
    }
    

