﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{

    public partial class LoginForm : Window
    {
        public LoginForm()
        {
            InitializeComponent();
        }
        

        
        private void ButtonSignUp_Click(object sender, RoutedEventArgs e)
        {
            RegistrationForm registrationForm = new RegistrationForm();
            registrationForm.Show();
            this.Close();

        }
        private void ButtonSignIn_Click(object sender, RoutedEventArgs e)
        {
                SearchForm searchForm = new SearchForm();
                searchForm.Show();
                this.Close();

        }

      
    }

}
