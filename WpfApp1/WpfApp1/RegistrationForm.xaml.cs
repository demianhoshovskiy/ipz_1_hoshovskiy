﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for RegistrationForm.xaml
    /// </summary>
    public partial class RegistrationForm : Window
    {
        public RegistrationForm()
        {
            InitializeComponent();
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
            this.Close();
        }

        private void ButtonSignUp_Click(object sender, RoutedEventArgs e)
        {
            bool containsInt = enterName.Text.Any(char.IsDigit);


            if (enterLogin.Text == "" || enterPassword.Password == "" || enterName.Text == "")
            {
                MessageBox.Show("Enter data");
                return;
            }

            if (enterName.Text == "")
            {
                MessageBox.Show("Invalid name");
                    return;
            }

            if (enterLogin.Text == "")
            {
                MessageBox.Show("Invalid login");
                return;
            }


            if (enterPassword.Password == "")
            {
                MessageBox.Show("Invalid password");
                return;
            }

            if (containsInt == true)
            {
                MessageBox.Show("Invalid name");
                return;
            }


           
            SearchForm searchForm = new SearchForm();
            searchForm.Show();
            this.Close();

        }


        private void enterName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void enterLogin_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}