﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for FinaleForm.xaml
    /// </summary>
    public partial class FinaleForm : Window
    {
        public FinaleForm()
        {
            InitializeComponent();
        }

        private void ButtonSignOut_Click(object sender, RoutedEventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
            this.Close();
        }

        private void ButtonGB_Click(object sender, RoutedEventArgs e)
        {
            SearchForm searchForm = new SearchForm();
            searchForm.Show();
            this.Close();
        }
    }
}
