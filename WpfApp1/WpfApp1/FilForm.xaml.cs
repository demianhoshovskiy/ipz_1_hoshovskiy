﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for FilForm.xaml
    /// </summary>
    public partial class FilForm : Window
    {


        public FilForm()
        {
            InitializeComponent();
            comboType.Items.Clear();

        }


            private void ButtonSignOut_Click(object sender, RoutedEventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
            this.Close();
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            SearchForm searchForm = new SearchForm();
            searchForm.Show();
            this.Close();
        }

        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {

        }

       

        private void ButtonCart_Click(object sender, RoutedEventArgs e)
        {

            if (sum.Text == "")

            {
                MessageBox.Show("Invalid request");
                return;
            }

            CheckOutForm checkOutForm = new CheckOutForm();
            checkOutForm.Cos.Text = this.sum.Text;

                checkOutForm.Show();
                this.Close();

        }

        private void dgg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid Dg = (DataGrid)sender;
            DataRowView row_selected = Dg.SelectedItem as DataRowView;
            if (row_selected != null)
            {

                sum.Text = row_selected["Ціна"].ToString();
            }
        }

        private void comboType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
