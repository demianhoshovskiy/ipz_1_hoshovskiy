﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for CheckOutForm.xaml
    /// </summary>
    public partial class CheckOutForm : Window
    {
        public CheckOutForm()
        {
            InitializeComponent();
        }

        


        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            SearchForm searchForm = new SearchForm();
            searchForm.Show();
            this.Close();
        }

        private void ButtonSignOut_Click(object sender, RoutedEventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
            this.Close();
        }

        private void ButtonCont_Click(object sender, RoutedEventArgs e)
        {

            bool isIntCard = card.Text.All(char.IsDigit);
            bool isIntCvv = cvv.Text.All(char.IsDigit);
            int cardLength = card.Text.Length;
            int cvvLength = cvv.Text.Length;
            bool containsInt = name.Text.Any(char.IsDigit);


            if (name.Text == "" || card.Text == "" || cvv.Text == "")
            {
                MessageBox.Show("Invalid data");
                return;
            }

            if (containsInt == true)
            {
                MessageBox.Show("Invalid name");
                return;
            }


            if (isIntCard == false)
            {
                MessageBox.Show("Invalid card number");
                return;
            }


            if (isIntCvv == false)
            {
                MessageBox.Show("Invalid cvv code");
                return;
            }


            if (cardLength != 16)
            {
                MessageBox.Show("Invalid card number");
                return;
            }


            if (cvvLength != 3)
            {
                MessageBox.Show("Invalid cvv code");
                return;
            }

            


            FinaleForm finaleForm = new FinaleForm();
            finaleForm.Show();
            this.Close();
        }
    }
}
