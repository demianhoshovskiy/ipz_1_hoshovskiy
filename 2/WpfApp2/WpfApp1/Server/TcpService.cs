﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Text.Json;

namespace Server
{
    public class SocketNode
    {
        public string Method { get; set; }
        public string Args { get; set; }
    }
    public class NewStatus
    {
        public int CarId { get; set; }
        public int Status { get; set; }
    }

    public class TcpService
    {
        private readonly DataService dataService;
        public TcpService(DataBase context = null)
        {
            if (context != null)
                dataService = new DataService(context);
        }


        public string DecodeAndProcessRequest(string request)
        {
            var socketNode = JsonSerializer.Deserialize<SocketNode>(request);
            TcpMethods tcpMethod;
            string response = "";
            if (!Enum.TryParse<TcpMethods>(socketNode.Method, out tcpMethod))
            {
                tcpMethod = TcpMethods.NONE;
            }

            switch (tcpMethod)
            {
                case TcpMethods.Authorize:
                    response = Authorize(socketNode);
                    break;
                case TcpMethods.AddUser:
                    response = AddUser(socketNode);
                    break;
                case TcpMethods.SearchByName:
                    response = SearchByName(socketNode);
                    break;
                case TcpMethods.SearchByProd:
                    response = SearchByProd(socketNode);
                    break;
                case TcpMethods.ShowProd:
                    response = ShowProd(socketNode);
                    break;
                case TcpMethods.ShowType:
                    response = ShowType(socketNode);
                    break;
                case TcpMethods.SearchByType:
                    response = SearchByType(socketNode);
                    break;
                default:
                    break;
            }

            return response;
        }

        public async Task<string> DecodeStreamAsync(NetworkStream stream)
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = await stream.ReadAsync(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString();
        }

        public string DecodeStream(NetworkStream stream)
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = stream.Read(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString();
        }

        public async Task<byte[]> CodeStreamAsync(string request)
        {
            return await Task.Run(() => CodeStream(request));
        }

        public byte[] CodeStream(string request)
        {
            return Encoding.UTF8.GetBytes(request);
        }

        public string SerializeAuthorizeRequest(string login, string password)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "Authorize",
                Args = JsonSerializer.Serialize<Users>(new Users
                {
                    Login = login,
                    Password = password
                })
            });
        }

        private string Authorize(SocketNode node)
        {
            string response = "";
            Users requestUser = JsonSerializer.Deserialize<Users>(node.Args);

            if (requestUser != null)
            {
                Users user = dataService.GetUser(requestUser.Login, requestUser.Password);
                if (user == null)
                {
                    user = new Users();
                }
                response = JsonSerializer.Serialize<Users>(user);
            }

            return response;
        }

        public Users DeserializeAuthorizeResponse(string response)
        {
            return JsonSerializer.Deserialize<Users>(response);
        }


        public string SerializeAddUserRequest(Users newUser)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "AddUser",
                Args = JsonSerializer.Serialize<Users>(newUser)
            });
        }

        private string AddUser(SocketNode socketNode)
        {
            try
            {
                Users user = JsonSerializer.Deserialize<Users>(socketNode.Args);
                dataService.InsertUser(user);
                return "1";
            }
            catch (Exception ex)
            {
                return "0;" + ex.Message;
            }
        }

        
        
        public string SerializeSearchByName(string name)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "SearchByName",
                Args = JsonSerializer.Serialize<string>(name)
            });
        }

        private string SearchByName(SocketNode socketNode)
        {
            string response = "";
            string name = JsonSerializer.Deserialize<string>(socketNode.Args);
            List<MedicinesInfo> medic = dataService.SearchByName(name);
            response = JsonSerializer.Serialize<List<MedicinesInfo>>(medic);
            return response;
        }

        public List<MedicinesInfo> DeserializeSearch(string response)
        {
            return JsonSerializer.Deserialize<List<MedicinesInfo>>(response);
        }

        public string SerializeSearchByProd(string prod)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "SearchByProd",
                Args = JsonSerializer.Serialize<string>(prod)
            });
        }

        private string SearchByProd(SocketNode socketNode)
        {
            string response = "";
            string prod = JsonSerializer.Deserialize<string>(socketNode.Args);
            List<MedicinesInfo> medic = dataService.SearchByProd(prod);
            response = JsonSerializer.Serialize<List<MedicinesInfo>>(medic);
            return response;
        }

        public string SerializeShowProd()
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "ShowProd"
            });
        }

        private string ShowProd(SocketNode socketNode)
        {
            string response = "";
            List<string> prod = dataService.ShowProd();
            response = JsonSerializer.Serialize<List<string>>(prod);
            return response;
        }

        public string SerializeShowType()
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "ShowType"
            });
        }

        private string ShowType(SocketNode socketNode)
        {
            string response = "";
            List<string> type = dataService.ShowType();
            response = JsonSerializer.Serialize<List<string>>(type);
            return response;
        }

        public string SerializeSearchByType(string type)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "SearchByType",
                Args = JsonSerializer.Serialize<string>(type)
            });
        }

        private string SearchByType(SocketNode socketNode)
        {
            string response = "";
            string type = JsonSerializer.Deserialize<string>(socketNode.Args);
            List<MedicinesInfo> medic = dataService.SearchByType(type);
            response = JsonSerializer.Serialize<List<MedicinesInfo>>(medic);
            return response;
        }

        public List<string> DeserializeShow(string response)
        {
            return JsonSerializer.Deserialize<List<string>>(response);
        }
    }



    public enum TcpMethods
    {
        NONE,
        Authorize,
        AddUser,
        SearchByName,
        SearchByProd,
        ShowProd,
        ShowType,
        SearchByType
    }
}
