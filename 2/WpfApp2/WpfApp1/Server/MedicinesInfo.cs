namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MedicinesInfo")]
    public partial class MedicinesInfo
    {
        [Key]
        public int MedicinesId { get; set; }

        [StringLength(101)]
        public string Producer { get; set; }

        [StringLength(101)]
        public string Name { get; set; }

        [StringLength(101)]
        public string Type { get; set; }

        public int? Price { get; set; }
    }
}
