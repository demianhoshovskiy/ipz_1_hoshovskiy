﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Server;


namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for SearchForm.xaml
    /// </summary>
    public partial class SearchForm : Window
    {
        private LoginForm loginForm;
        private readonly TcpService tcpService;
        public SearchForm(LoginForm loginForm)
        {
            InitializeComponent();
            this.loginForm = loginForm;
            tcpService = new TcpService();
        }


        public async void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string request = " ";
                if (nameTxt.Text == "" & manTxt.Text == "")
                {
                    throw new ArgumentException("Поле не може бути пусте");
                }

                if (nameTxt.Text == "" == false & manTxt.Text == "" == false)
                {
                    throw new ArgumentException("Використовуйте лише один тип пошуку");
                }

                if (nameTxt.Text == "" == false)
                {
                    request = tcpService.SerializeSearchByName(nameTxt.Text);
                }
                if (manTxt.Text == "" == false)
                {
                    request = tcpService.SerializeSearchByProd(manTxt.Text);

                }

                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                List<MedicinesInfo> medic = tcpService.DeserializeSearch(response);

                if (medic.Count == 0 && (manTxt.Text == "" != false))
                {
                    throw new ArgumentException("Такого препарату немає");
                }
                else if (medic.Count == 0 && (nameTxt.Text == "" != false))
                {
                    throw new ArgumentException("Такого виробника немає");
                }
                else
                {
                    Dg.ItemsSource = medic;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonFil_Click(object sender, RoutedEventArgs e)
        {
            FilForm filForm = new FilForm(this);
            filForm.Show();
            this.Hide();
        }

        private void cartTxt_Click(object sender, RoutedEventArgs e)
        {

            if (price.Text == "")

            {
                MessageBox.Show("Не вибрано жодного товару");
                return;
            }

            CheckOutForm checkOutForm = new CheckOutForm(this);
            checkOutForm.Cos.Text = this.price.Text;

            checkOutForm.Show();
            this.Hide();

        }

        private void Dg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid Dg = (DataGrid)sender;
            if (Dg.SelectedItem != null)
            {
                MedicinesInfo md = Dg.SelectedItem as MedicinesInfo;
                price.Text = md.Price.ToString();
            }
        }

        private void ButtonSignOut_Click(object sender, RoutedEventArgs e)
        {
            loginForm.Back(false);
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            loginForm.Back(true);
        }
    }

}


