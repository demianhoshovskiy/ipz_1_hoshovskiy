﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for CheckOutForm.xaml
    /// </summary>
    public partial class CheckOutForm : Window
    {
        private SearchForm sForm;
        public CheckOutForm(SearchForm sForm)
        {
            InitializeComponent();
            this.sForm = sForm;
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            sForm.Show();
            this.Close();
        }


        private void ButtonCont_Click(object sender, RoutedEventArgs e)
        {

            bool isIntCard = card.Text.All(char.IsDigit);
            bool isIntCvv = cvv.Text.All(char.IsDigit);
            int cardLength = card.Text.Length;
            int cvvLength = cvv.Text.Length;
            bool containsInt = name.Text.Any(char.IsDigit);


            if (name.Text == "" || card.Text == "" || cvv.Text == "")
            {
                MessageBox.Show("Поля не можуть бути пусті");
                return;
            }

            if (containsInt == true)
            {
                MessageBox.Show("Хибне ім'я");
                return;
            }


            if (isIntCard == false)
            {
                MessageBox.Show("Хибний номер карти");
                return;
            }


            if (isIntCvv == false)
            {
                MessageBox.Show("Хибний CVV код");
                return;
            }


            if (cardLength != 16)
            {
                MessageBox.Show("Хибний номер карти");
                return;
            }


            if (cvvLength != 3)
            {
                MessageBox.Show("Хибний CVV код");
                return;
            }

            MessageBox.Show("Платіж успішний");
            sForm.Show();
            this.Close();
        }
    }
}
