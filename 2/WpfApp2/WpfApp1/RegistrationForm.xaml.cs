﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Server;


namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for RegistrationForm.xaml
    /// </summary>
    public partial class RegistrationForm : Window
    {
        private LoginForm loginForm;
        private readonly TcpService tcpService;
        public RegistrationForm(LoginForm loginForm)
        {
            InitializeComponent();
            this.loginForm = loginForm;
            tcpService = new TcpService();
        }

        protected override void OnClosed(EventArgs e)
        {
            loginForm.Back(true);
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            loginForm.Back(false);
            this.Close();
        }

        private async void ButtonSignUp_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                inputNotNull(enterLogin.Text);
                inputNotNull(enterPassword.Password);
                inputNotNull(enterName.Text);

                if (enterName.Text.Any(char.IsDigit))
                {
                    throw new ArgumentException("Ім'я не може містити числа");
                }

                var requestUser = new Users
                {
                    Login = enterLogin.Text,
                    Password = enterPassword.Password,
                    UserName = enterName.Text
                };


                string request = tcpService.SerializeAddUserRequest(requestUser);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                var responseArgs = response.Split(';');
                if (responseArgs.Length > 1 && responseArgs[0].Contains("0"))
                {
                    throw new ArgumentException(responseArgs[1]);
                }
                if (responseArgs.Contains("1"))
                {
                    MessageBox.Show("Реєстрація пройшла успішно");
                }

                loginForm.Back(false);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public static void inputNotNull(string st)
        {
            if (st.Length <= 0)
            {
                throw new ArgumentException("Поля не можуть бути пусті");
            }
        }
    }
}